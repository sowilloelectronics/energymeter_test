## Energy Meter test bench


---
**Sketch FlowMeter_ArduinoMega for testing Modbus on a chip Arduino Mega**

Testing of a new chip max13487, with automatic detection of the transmission direction, without pins PE DE. Power supply max13487 - 5V, communication with arduino Mega.

---


## Installing MQTT broker Mosquitto on Raspberry Pi

1. Updating the system. 
First of all, before starting the installation, we need to update the system.
```
		sudo apt-get update
		sudo apt-get upgrade
```

2. Installation of broker and client "mosquitto". 
The regular Raspberry Pi (Orange Pi) “apt-get” libraries do not contain the latest Mosquitto software. Therefore, before installing, we will need to update the libraries. If not the latest version of the broker is installed on the server, errors may appear during operation.
Add the key and update the repository. We enter these lines in turn. Sometimes you will need to enter the password from your user and press Y to agree with the installation.
```
		sudo wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
		sudo apt-key add mosquitto-repo.gpg.key
		cd /etc/apt/sources.list.d/
		sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
		sudo apt-get update
```

3. Install the MQTT broker (server):
```
		sudo apt-get install mosquitto
```

3. Install the MQTT client:
```
		sudo apt-get install mosquitto mosquitto-clients
```

4. Stopping the server to configure.

```
		sudo /etc/init.d/mosquitto stop
```

5. Customization. Opening the settings file
```
		sudo nano /etc/mosquitto/mosquitto.conf
```
and replace its contents with the following:
```
		# Place your local configuration in /etc/mosquitto/conf.d/
		#
		# A full description of the configuration file is at
		# /usr/share/doc/mosquitto/examples/mosquitto.conf.example

		pid_file /var/run/mosquitto.pid

		persistence true
		persistence_location /var/lib/mosquitto/

		log_dest topic

		log_type error
		log_type warning
		log_type notice
		log_type information

		connection_messages true
		log_timestamp true

		include_dir /etc/mosquitto/conf.d
```
Save, close the nano program and exit back to the terminal (ctrl + o, Enter, ctrl + x).

6. Start the MQTT server:
```
		sudo /etc/init.d/mosquitto start
```

7. Checking the work of the MQTT broker
```
		mosquitto_sub -d -t sowillo/data
		mosquitto_pub -d -t sowillo/data -m "Data"
```

8. MQTT server status
You can always check the status of your MQTT broker through the terminal:
```
		sudo /etc/init.d/mosquitto status
```

9. Setting a password for MQTT
it is necessary to create a configuration file that will contain the username and encrypted password.
```
		sudo mosquitto_passwd -c /etc/mosquitto/passwd <username>
```
you need to change the system access settings, for this you need to edit the /etc/mosquitto/conf.d/default.conf file on your pi.
```
		sudo nano /etc/mosquitto/conf.d/default.conf
```
In the file that opens, add 2 lines:
```
		allow_anonymous false
		password_file /etc/mosquitto/passwd
```

10. Restarting the MQTT server
```
		sudo systemctl restart mosquitto
```
---

## Installing QTMQTT for QTcreater on Raspberry Pi

To use a Qt module, it must be installed in the Qt lib directory.
First, retrieve the lib directory path executing this command from a terminal:
```
		qmake -query QT_INSTALL_LIBS
```

Cd into that directory and check if a file called Qt5Mqtt.dll is there: if not, you must build/install the module.
To get the module source code, you can execute this git command:
```		
		cd <projects folder>
		git clone git://code.qt.io/qt/qtmqtt.git
```

Once you have the source files, cd into the source files directory containing the file qtmqtt.pro and run these commands:
```
		cd qmqtt
		mkdir build
		cd build
		qmake -r ..
		sudo make install
```
Link path to include dirrectory QT Creator

Done

To link against the module, add this line to your qmake .pro file:

```
		QT += mqtt
```
		